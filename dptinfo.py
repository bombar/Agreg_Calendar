#!/usr/bin/python3
#-*- mode:python3 -*-

import bs4 as BeautifulSoup
import urllib3

import datetime

from icalendar import Calendar, Event

import uuid
from pyjsparser import PyJsParser

def new_calendar():
    c = Calendar()
    c.add('prodid', '-//Maxime Bombar//Agreg Maths ENS Cachan//FR') # RFC compliance
    c.add('version', '2.0') # RFC compliance
    return c

def get_elements():
    http = urllib3.PoolManager()
    request = http.request('GET', 'http://www.dptinfo.ens-cachan.fr/Agregation/edt/edt.html')
    html = request.data
    soup = BeautifulSoup.BeautifulSoup(html, 'lxml') # Create the soup
    a = soup.find_all('script')[-1]
    p = PyJsParser() 
    d = p.parse(a.string)
    elements = d['body'][0]['expression']['arguments'][0]['body']['body'][0]['expression']['arguments'][0]['properties'][5]['value']['elements']
    return elements

def get_time(element):
    start = datetime.datetime.strptime(
        element['properties'][1]['value']['value'], '%Y-%m-%dT%H:%M:%S'
    )
    end = datetime.datetime.strptime(
        element['properties'][2]['value']['value'], '%Y-%m-%dT%H:%M:%S'
    )
    return start, end

def get_summary(element):
    return element['properties'][3]['value']['value']

def main():
    c = new_calendar()
    elements = get_elements()
    
    for element in elements:
        start, end = get_time(element)
        e = Event()
        e.add('summary', get_summary(element))
        e.add('dtstart', start)
        e.add('dtend', end)
        e.add('dtstamp', datetime.datetime.now())
        e.add('uid', uuid.uuid1().hex)
        
        c.add_component(e)
    
    return c.to_ical()

if __name__ == '__main__':
    print(main().decode('utf-8'))
