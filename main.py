#!/usr/bin/python3
#-*- mode:python3 -*-

"""
Dependencies :
   - BeautifulSoup
   - icalendar
"""

import sys

import bs4 as BeautifulSoup
import urllib3

import datetime

from icalendar import Calendar, Event

import re

import argparse

import uuid

# today's week beginning date
today = datetime.datetime.strptime(
    datetime.datetime.now().strftime('%y-W%W') + '-1', "%y-W%W-%w"
).strftime('%y-%m-%d')


def new_calendar():
    c = Calendar()
    c.add('prodid', '-//Maxime Bombar//Agreg Maths ENS Cachan//FR') # RFC compliance
    c.add('version', '2.0') # RFC compliance
    return c

def get_schedule(a):
    """
    returns a list of the schedule for the 5 days of the week
    from schedule website
    """
    schedule = []
    for day in a:
        schedule.append(day.find_all("big"))
    return schedule

def get_string(tag):
    """ Get string from BeautifulSoup tag, itering on children if there are more than 1"""
    if len(list(tag.children)) > 1:
        s = ''
        for x in tag.children:
            if x.string.replace('\xa0', ''):
                s += x.string
                s += ' '
    else:
        s = tag.string
    return s

def pretty_schedule(schedule):
    """Prettify the schedule"""
    days = []
    for day in schedule:
        cls = []
        i = 0
        while i<len(day):
            j = 1
            s = get_string(day[i])
            if s and regex.match(s) or s == 'Problème': # Beginning of a class
                tmp = [day[i]]
                try:
                    while get_string(day[i+j]) and not regex.match(get_string(day[i+j])):
                        if get_string(day[i+j]).replace('\xa0', ''):
                            tmp.append(day[i+j])
                        j+=1
                except IndexError:
                    pass
                cls.append(tmp)
            i+=j
        days.append(cls)
    return format(days)


def format(days):
    """
    days: list of list.
    Each day is a sublist which contains the classes for this day.
    Format everything from BeautifulSoup tags to strings, into a list of list of dict
    """
    pretty_days = []
    for i in range(len(days)):
        day = []
        for cls in days[i]:
            clas = {}
            try:
                clas['time'] = get_string(cls[0])
            except:
                pass
            try:
                clas['type'] = get_string(cls[1])
            except:
                pass
            try:
                clas['topic'] = get_string(cls[2])
            except:
                pass
            try:
                clas['prof'] = get_string(cls[3])
            except:
                pass
            try:
                clas['room'] = get_string(cls[4])
            except:
                pass
            for i in range(len(cls[5:])):
                try:
                    clas['title_%s' % i] = get_string(cls[5:][i]).replace('č', 'è')
                except:
                    pass
            day.append(clas)
        pretty_days.append(day)
    return pretty_days


def pretty_print(days, date):
    """
    Display the week schedule
    """
    for i in range(len(days)):
        day_date = datetime.datetime.strptime(
            datetime.datetime.strptime(
                date, '%y-%m-%d'
            ).strftime('%y-W%W') + '-%s' % (i%6+1), '%y-W%W-%w'
        ).strftime("%A %B %d, %Y")

        print("----------------------------------")
        print("   %s:" % day_date)
        print("----------------------------------")
        for clas in days[i]:
            try:
                print(clas['time'])
            except KeyError:
                pass
            try:
                print(clas['type'])
            except KeyError:
                pass
            try:
                print(clas['topic'])
            except KeyError:
                pass
            try:
                print(clas['prof'])
            except KeyError:
                pass
            try:
                print(clas['room'])
            except KeyError:
                pass
            try:
                for cat in clas:
                    if 'title' in cat:
                        print(clas[cat])
            except KeyError:
                pass
            print("\n")
        print('\n')

def get_time(clas):
    time = clas['time']
    l = re.findall(r"[\w']+", time)
    return l[0], l[1]

def fill_calendar(c, days, date):
    """
    Fill calendar with events for each class
    """
    for i in range(len(days)):
        day_date = datetime.datetime.strptime(
            datetime.datetime.strptime(
                date, '%y-%m-%d'
            ).strftime('%y-W%W') + '-%s' % (i%6+1), '%y-W%W-%w'
        )


        for clas in days[i]:
            e = Event()

            start, end = get_time(clas)

            try:
                h_start = int(start.split('h')[0])
            except:
                h_start = 0
            try:
                m_start = int(start.split('h')[1])
            except:
                m_start = 0
            try:
                h_end = int(end.split('h')[0])
            except:
                h_end = 0
            try:
                m_end = int(end.split('h')[1])
            except:
                m_end = 0

            try:
                e.add('location', clas['room'])
            except:
                pass
            summary = ''
            for cat in clas:
                if cat not in ['time', 'room']:
                    try:
                        summary += clas[cat] + '\n'
                    except:
                        pass

            e.add('summary', summary)

            dtstart = day_date.replace(hour=h_start, minute=m_start)
            dtend = day_date.replace(hour=h_end, minute=m_end)
            e.add('dtstart', dtstart)
            e.add('dtend', dtend)
            e.add('dtstamp', datetime.datetime.now())
            e.add('uid', uuid.uuid1().hex)

            c.add_component(e)


def parse_schedule(request):
    """
    Parse the schedule found in the request
    """
    html = request.data
    soup = BeautifulSoup.BeautifulSoup(html, 'lxml') # Create the soup
    a = soup.find_all('table', cellspacing="0")[2:]
    schedule = get_schedule(a)
    days = pretty_schedule(schedule)
    return days

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fetch calendar from agreg.cmla.ens-cachan.fr', add_help=True)
    parser.add_argument(
        "-d", "--date",
        help="Date From in format yy-mm-dd",
        action="store",
        default = today
    )

    parser.add_argument(
        "-p", "--pretty",
        help="Pretty print",
        action="store_true",
        )

    parser.add_argument(
        "-i", "--ics",
        help="export ics",
        action="store_true",
    )

    parser.add_argument(
        "-a", "--all",
        help="export the whole year",
        action="store_true",
    )

    parser.add_argument(
        "-o", "--option",
        help="specifies the option. Available are pr, cs, ae and in",
        action="store",
        default = "in",
)


    options = ["pr", "cs", "ae", "in"]

    args = parser.parse_args()

    if args.option not in options:
        parser.print_help(sys.stderr)
        sys.exit(42)

    date = datetime.datetime.strptime(
        datetime.datetime.strptime(
            args.date, '%y-%m-%d'
        ).strftime('%y-W%W') + '-1', "%y-W%W-%w")

    http = urllib3.PoolManager()

    regex = re.compile('[0-9]*h[0-9]*-[0-9]*h[0-9]*') # regex time

    if not args.all:
        date = date.strftime('%y-%m-%d')
        request = http.request('GET', 'http://agreg.cmla.ens-cachan.fr/edt.php?option=%(option)s&sem=%(date)s&suff=' % {'option': args.option, 'date': date} )

        days = parse_schedule(request)

        if args.pretty:
            pretty_print(days, date)

        else:
            c = new_calendar() # RFC Compliance
            fill_calendar(c, days, date)
            print(c.to_ical().decode('utf-8'))

    else:
        # fetch every week !
        c = new_calendar() # RFC Compliance
        while date.month < 7 or date.year == 2018:
            date_str = date.strftime('%y-%m-%d')

            request = http.request('GET', 'http://agreg.cmla.ens-cachan.fr/edt.php?option=%(option)s&sem=%(date)s&suff=' % {'option': args.option, 'date': date_str} )

            days = parse_schedule(request)

            fill_calendar(c, days, date_str)

            date += datetime.timedelta(weeks=1)

        print(c.to_ical().decode('utf-8'))
